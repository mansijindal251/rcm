import React, { useContext } from 'react'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';
import { ParticularServiceContext } from '../context/ParticularServiceContext';
import { Link } from 'react-router-dom';

const SubServiceContent = () => {
    const { serviceDetail, isDisabled, isDisabledNext, index, relatedServices } = useContext(ParticularServiceContext);
    return (
        <>
            <div>
                <div className='container mx-auto'>
                    <div style={{ animation: 'slideFromTop 1s ease' }} className='grid grid-cols-1 md:grid-cols-2  xl:grid-cols-4 gap-4 px-2'>

                        {serviceDetail.subServices?.map((subSubserviceDetails) => (

                            <div
                                className="cursor-pointer overflow-auto relative transition-all duration-500 hover:translate-y-2 w-full h-full bg-neutral-50 rounded shadow-xl flex items-center justify-evenly gap-2 px-8 xl:px-12 py-8 before:absolute before:w-full hover:before:top-0 before:duration-500 before:-top-1 before:h-1 before:bg-green-500"
                            >
                                <div>
                                    <div className='pb-4'>
                                        <img className='p-1 bg-green-500 flex w-12 rounded' src={subSubserviceDetails.subServiceIcon} />
                                    </div>
                                    <div>
                                        <span class="font-bold">{subSubserviceDetails.subServiceTitle}</span>
                                        <p className='pb-6 pt-2'>
                                            {subSubserviceDetails.subServiceDescription}
                                        </p>
                                        <button class="cursor-pointer font-semibold overflow-hidden relative border rounded border-green-500 group px-4 py-1">
                                            <span class="relative z-10 color-green group-hover:text-white text-lg duration-500">Read More</span>
                                            <span class="absolute w-full h-full bg-green -left-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:left-0 duration-500"></span>
                                            <span class="absolute w-full h-full bg-green -right-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:right-0 duration-500"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                    {/* Next and the Previous Buttons */}

                    <div className='grid grid-cols-1 place-items-center  md:flex justify-between pb-12'>

                        {/* {!isDisabled && ( */}
                        <Link to={`/services/${index == 1 && relatedServices?.length == 3 ? relatedServices[0]?.serviceSlug : relatedServices[0]?.serviceSlug}`} style={{ animation: 'slideFromRight 1s ease' }} className="btn mt-12">
                            <div><KeyboardDoubleArrowLeftIcon /> Previous Service</div>
                        </Link>
                        {/* )} */}

                        <Link to={`/services/${index == 1 && relatedServices?.length == 3 ? relatedServices[2]?.serviceSlug : relatedServices[1]?.serviceSlug}`} style={{ animation: 'slideFromLeft 1s ease' }} className="btn mt-12">
                            <div>Next Service <KeyboardDoubleArrowRightIcon /></div>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SubServiceContent