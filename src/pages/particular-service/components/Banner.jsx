import React from 'react'
import HomeIcon from '@mui/icons-material/Home';
import { headContentAnimation } from '../../../utils/motion';
import { motion } from "framer-motion";
import { Link } from 'react-router-dom';
import { ParticularServiceContext } from '../context/ParticularServiceContext';

const Banner = () => {
    const { serviceDetail } = React.useContext(ParticularServiceContext)
    return (
        <>
            <div className='hero-section'>
                <div className='py-20 md:py-24 xl:px-32 2xl:px-52'>
                    <motion.div {...headContentAnimation}>
                        <div className='pl-1.5 pb-6 flex items-center gap-4'>
                            <Link to="/">
                                <button className="Btn">
                                    <div className="sign">
                                        <HomeIcon />
                                    </div>
                                    <div className="text">Home</div>
                                </button>
                            </Link>
                            <div className=" w-32 border-b-2 border-green-500">
                            </div>
                            <span className='text-xl md:text-3xl color-white font-bold  uppercase'>Services</span>
                        </div>
                        <span className='text-5xl md:text-6xl font-bold color-white'>{serviceDetail.serviceTitle}
                        </span>
                        <div className='pt-4 pl-1.5 w-full md:w-2/3 xl:w-2/5'>
                            <span className='text-lg font-medium color-white'> {serviceDetail.serviceDescription}
                            </span>
                        </div>
                    </motion.div>
                </div>
            </div>
        </>
    )
}

export default Banner