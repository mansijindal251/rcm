import React from 'react'
import Banner from './Banner'
import SubServiceContent from './SubServiceContent'
import { useParams } from 'react-router-dom';
import { ParticularServiceContext } from '../context/ParticularServiceContext';
import "../styles.css"

const ParticularService = () => {
    const { getServiceByName } = React.useContext(ParticularServiceContext)
    const { serviceSlug } = useParams();

    React.useEffect(() => {
        getServiceByName(serviceSlug)
    }, [serviceSlug]);

    return (
        < >
            <Banner />
            <SubServiceContent />
        </>
    )
}

export default ParticularService