import HttpClient from "../../../utils/HttpClient";

export async function getSingleService(data) {
    try {
        const queryParams = {
            serviceSlug: data
        }
        const response = await HttpClient.get('services/get-service', queryParams)
        return response.data
    } catch (err) {
        console.log("error in the particular Service API file", err)
    }
}