import React, { createContext, useState } from 'react';
import { getSingleService } from '../repository/ParticularServiceAPI';

export const ParticularServiceContext = createContext();

export const ParticularServiceProvider = ({ children }) => {

    const [serviceDetail, setServiceDetail] = React.useState({ service: {} });
    const [index, setIndex] = useState(-1);
    const [isDisabled, setIsDisabled] = React.useState(false);
    const [isDisabledNext, setIsDisabledNext] = React.useState(false);
    const [relatedServices, setRelatedServices] = React.useState([]);

    const getServiceByName = async (data) => {
        try {
            const responseData = await getSingleService(data);
            if (responseData) {
                setServiceDetail(responseData.service);
                setRelatedServices(responseData.relatedServices)

                // To Find The Index of The Service 
                let relatedServices = responseData.relatedServices

                for (let i = 0; i < relatedServices.length; i++) {
                    if (relatedServices[i].serviceSlug == data) {
                        setIndex(i)
                        break;
                    }
                }
            }
        }
        catch (err) {
            console.log("error", err)
        }
    }

    React.useEffect(() => {
        if (index == 0 && relatedServices.length == 2) {
            setIsDisabled(true)
        }
        else if (index == 1 && relatedServices.length == 2) {
            setIsDisabledNext(true)
            console.log("isDisabled of else sattement", isDisabledNext, index)
        }
        else {
            setIsDisabled(false)
            setIsDisabledNext(false)
        }
    }, [isDisabled, isDisabledNext, index]);

    return (
        <ParticularServiceContext.Provider
            value={{
                getServiceByName,
                serviceDetail,
                index,
                isDisabled,
                isDisabledNext,
                relatedServices
            }}
        >
            {children}
        </ParticularServiceContext.Provider>
    )
}
