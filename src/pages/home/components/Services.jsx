import React from 'react'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import Tooltip from '@mui/material/Tooltip';
import { Link } from 'react-router-dom';
import { HomeContext } from '../context/HomeContext';
const Services = () => {
    const { getAllServices, services } = React.useContext(HomeContext);
    React.useEffect(
        () => {
            getAllServices()
        }, []
    )
    return (
        <>
            <div className='pb-6 pt-24 md:pt-20 lg:pt-0 md:pb-10 text-white text-4xl md:text-5xl text-center md:text-left '>
                <span className='font-bold'>Our </span>
                <span className='font-bold'>Services</span>
            </div>

            <div className="overflow-x-auto xl:overflow-hidden flex-nowrap flex gap-4 relative w-full p-4 md:p-0">

                {/* Card-1 */}
                {services.map((data) => (
                    <Link key={data._id} to={`/services/${data.serviceSlug}`} className="!no-underline bg-grey flex-shrink-0 xl:flex-shrink rounded-xl overflow-hidden relative text-center p-8 group items-center flex flex-col w-full md:w-5/12 lg:w-1/4  h-60 hover:shadow-2xl transition-all duration-500 shadow-xl cursor-pointer">
                        <div className="text-gray-500 group-hover:scale-105 group-hover:pt-0 transition-all pt-4">
                            <img src={data.serviceIcon} />
                        </div>
                        <div className="group-hover:pb-10 transition-all duration-500 delay-200 pt-6">
                            <h1 className="font-semibold text-gray-700 ">{data.serviceTitle}</h1>
                            <a className="text-gray-500 hover:underline text-sm ">More Details <ArrowForwardIcon fontSize='small' /></a>
                        </div>
                        <div className="flex items-center transition-all duration-500  group-hover:bottom-3 -bottom-full absolute gap-2 justify-evenly w-full">
                            <div className="flex gap-3 text-2xl bg-navy text-white p-1 hover:p-2 transition-all duration-500 delay-200 rounded-full shadow-sm">
                                {data.subServices.map((subService) => (
                                    <Tooltip title={subService.subServiceTitle} key={subService._id}>
                                        <Link to={`/services/${data.serviceSlug}/${subService.subServiceSlug}`} className="hover:scale-110 transition-all duration-500 delay-200">
                                            <img className='w-6' src={subService.subServiceIcon} alt={subService.subServiceTitle} />
                                        </Link>
                                    </Tooltip>
                                ))}
                            </div>
                        </div>
                    </Link>
                ))}
            </div>
        </>
    )
}

export default Services