import React from 'react'
import { Carousel } from 'react-responsive-carousel';
import Services from './Services';
const Slider = () => {
    return (
        <>
            <div className='relative pb-8'>
                <Carousel showArrows={false} autoPlay={true} showThumbs={false} infiniteLoop={true} showStatus={false} transitionTime={1000}>
                    <div>
                        <div className="image-overlay" />
                        <img src='/assets/img_3.jpg' />
                    </div>
                    <div>
                        <div className="image-overlay" />
                        <img src='/assets/img_1.jpg' />
                    </div>
                    <div>
                        <div className="image-overlay" />
                        <img src='/assets/img_6.jpg' />
                    </div>
                </Carousel>
                <div className="absolute inset-0 lg:justify-center flex flex-col z-20 container mx-auto max-w-7xl md:max-w-2xl lg:max-w-4xl xl:max-w-7xl">
                    <Services />
                </div>
            </div>
        </>
    )
}

export default Slider