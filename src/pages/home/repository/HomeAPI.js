import HttpClient from "../../../utils/HttpClient";

// getAllServices
export async function getServices() {
    try {
        const response = await HttpClient.get('/services/get-services');
        if (response) {
            return response.data.services
        }

    } catch (err) {
        console.log("error in the repo API File")
    }
}