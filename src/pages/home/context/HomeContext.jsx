import React, { useState, createContext } from "react";
import { getServices } from "../repository/HomeAPI";

export const HomeContext = createContext();

export const HomeProvider = ({ children }) => {
    const [services, setServices] = React.useState([])

    const getAllServices = async () => {
        try {
            const responseData = await getServices();
            if (responseData) {
                setServices(responseData)
            }
        }
        catch (error) {
            console.log("error", error)
        }
    }
    return (
        <HomeContext.Provider
            value={{
                getAllServices,
                services
            }}
        >
            {children}
        </HomeContext.Provider>
    )
}