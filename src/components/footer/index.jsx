import React from 'react'
import Divider from '@mui/material/Divider';
import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <>
            <div className='relative bottom-0 w-full bg-color-lightest-grey px-2'>
                <div className="container mx-auto max-w-7xl py-10">
                    <div className='grid grid-cols-1 lg:grid-cols-4 pb-16 color-navy'>
                        <div>
                            <img className='w-24' src='/assets/cwj_logo.png' />
                        </div>

                        <div className='pt-8 lg:pt-0'>
                            <p className='text-2xl font-medium black-violet'>About Us</p>
                            <ul className='pt-2 light-grey text-lg'>
                                <a href=''><li>About Us</li></a>
                                {/* <a href=""><li>Influencers</li></a> */}
                                {/* <a href=''><li>Brand</li></a> */}
                                <a href=""><li>Contact Us</li></a>
                            </ul>
                        </div>

                        <div>
                            <p className='text-2xl font-medium black-violet pt-4 lg:pt-0'>Resources</p>
                            <ul className='pt-2 light-grey text-lg'>
                                <a href=""><li>Try Now</li></a>
                                <a href=""><li>Blow</li></a>
                                <a href=""><li>FAQ's</li></a>
                            </ul>
                        </div>

                        <div>
                            <p className='text-2xl font-medium black-violet pt-4 lg:pt-0'>Get In Touch</p>
                            <ul className='pt-2 light-grey text-lg'>
                                <li>Ph: +01234 567 890</li>
                                <li>Email: info@CWJHealth.com</li>
                                <li>Address: 5526 West 13400 South</li>
                            </ul>
                        </div>

                    </div>

                    <div className='pt-4'>
                        <Divider className='border-t-6' />
                    </div>
                    <div className='pt-6 text-center color-navy'>
                        <p>Made with 🧡 in United States © Copyright 2024 <Link to="www.techtweek.us">TechtweekInfotech</Link> Privacy Policy | Terms & Condition</p>
                    </div>
                </div>

            </div>
        </>
    )
}

export default Footer