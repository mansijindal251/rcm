import React, { createContext } from 'react';

// Context Name
export const HeaderContext = createContext();

export const HeaderProvider = ({ children }) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [reverse, setReverse] = React.useState(false);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        setReverse(true);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <HeaderContext.Provider
            value={{
                setReverse,
                handleClick,
                handleClose,
                reverse,
                open,
                anchorEl, setAnchorEl
            }}
        >
            {children}
        </HeaderContext.Provider>
    )
}