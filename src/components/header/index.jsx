import React from 'react'
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MenuIcon from '@mui/icons-material/Menu';
import "./styles.css"
const Header = () => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [reverse, setReverse] = React.useState(false);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        setReverse(true);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [menuOpen, setMenuOpen] = React.useState(false);

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };


    return (
        <>
            <div className='bg-green py-3'>
                <div className='container mx-auto max-w-7xl md:max-w-2xl lg:max-w-4xl xl:max-w-7xl'>
                    <div className='flex flex-wrap md:justify-between justify-center'>
                        <div className='grid place-content-center md:flex'>
                            <div className='flex items-center pb-1.5 md:pb-0'>
                                <img className='w-4' src='/assets/telephone.png' />
                                <span className='pl-1 font-medium color-white text-sm'>+01234 567 890 (24/7) &nbsp; <span className='font-bold'><span className='hidden md:inline'>|</span> </span>&nbsp;</span>
                            </div>
                            <div className='flex items-center  pb-1.5 md:pb-0'>
                                <img className='w-4' src='/assets/pin.png' />
                                <span className='pl-1 font-medium color-white text-sm'>5526 West 13400 South</span>
                            </div>
                        </div>
                        <div className='flex items-center'>
                            <img className='w-4' src='/assets/clock.png' />
                            <span className='pl-1.5 font-medium color-white text-sm'>Monday - Saturday: 9 am - 11.30 pm</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container mx-auto max-w-7xl md:max-w-2xl lg:max-w-4xl xl:max-w-7xl py-4'>
                <div className='flex color-navy w-full'>
                    <div className='px-8 md:px-0 flex items-center gap-4 w-full justify-between '>
                        <div className='md:flex items-center gap-3'>
                            <a href='/'>
                                <img className='md:w-24 w-20' src='/assets/cwj_logo.png' />
                            </a>
                            <div className='hidden md:grid lg:flex flex-col '>
                                <span className='text-lg  font-bold'>CWJ Health Solutions</span>
                                <span className='leading-4 text-xs'>Medical & Health Service</span>
                            </div>
                        </div>
                        <div className='flex gap-5 items-center'>
                            <div className='xl:hidden' onClick={toggleMenu}><MenuIcon></MenuIcon></div>
                            <button class="hidden md:inline xl:hidden cursor-pointer font-semibold overflow-hidden relative border rounded border-green-500 group px-4 py-1">
                                <span class="relative z-10 color-green group-hover:text-white text-lg duration-500">Contact Us</span>
                                <span class="absolute w-full h-full bg-green -left-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:left-0 duration-500"></span>
                                <span class="absolute w-full h-full bg-green -right-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:right-0 duration-500"></span>
                            </button>
                        </div>
                    </div>
                    <ul className='hidden text-lg font-semibold xl:flex justify-between w-full items-center'>
                        <li><button className='list-item'>Home</button></li>
                        <li class="paste-button">
                            <button class="button list-item">Specialities ▼</button>
                            <div class="dropdown-content">
                                <a id="top" href="#">Keep text only</a>
                                <a id="middle" href="#">Keep text only</a>
                                <a id="bottom" href="#">Keep text only</a>
                                <a id="bottom" href="#">Keep text only</a>
                            </div>
                        </li>
                        <li><button className='list-item'>About</button></li>
                        <li><button className='list-item'>Privacy Policy</button></li>
                        <li><button className='list-item'>Quality</button></li>
                        <button class="cursor-pointer font-semibold overflow-hidden relative border rounded border-green-500 group px-4 py-1">
                            <span class="relative z-10 color-green group-hover:text-white text-lg duration-500">Contact Us</span>
                            <span class="absolute w-full h-full bg-green -left-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:left-0 duration-500"></span>
                            <span class="absolute w-full h-full bg-green -right-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:right-0 duration-500"></span>
                        </button>
                    </ul>
                </div>
                {/* For Mobile Screens */}
                <ul className={`xl:hidden text-lg font-semibold flex flex-col justify-center w-full items-center ${menuOpen ? 'block' : 'hidden'}`}>
                    <li><button className='list-item'>Home</button></li>
                    <li class="paste-button">
                        <button class="button list-item">Specialities ▼</button>
                        <div class="dropdown-content">
                            <a id="top" href="#">Keep text only</a>
                            <a id="middle" href="#">Keep text only</a>
                            <a id="bottom" href="#">Keep text only</a>
                            <a id="bottom" href="#">Keep text only</a>
                        </div>
                    </li>
                    <li><button className='list-item'>About</button></li>
                    <li><button className='list-item'>Privacy Policy</button></li>
                    <li><button className='list-item'>Quality</button></li>
                    <button class="md:hidden cursor-pointer font-semibold overflow-hidden relative border rounded border-green-500 group px-4 py-1">
                        <span class="relative z-10 color-green group-hover:text-white text-lg duration-500">Contact Us</span>
                        <span class="absolute w-full h-full bg-green -left-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:left-0 duration-500"></span>
                        <span class="absolute w-full h-full bg-green -right-32 top-0 -rotate-45 group-hover:rotate-0 group-hover:right-0 duration-500"></span>
                    </button>
                </ul>
            </div>

            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
            </Menu>
        </>
    )
}

export default Header