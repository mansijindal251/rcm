import './App.css'
import { Routes, Route, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from "framer-motion";
import Home from './pages/home/components';
import Header from './components/header';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Footer from './components/footer';
import ParticularService from './pages/particular-service/components';
import { ParticularServiceProvider } from './pages/particular-service/context/ParticularServiceContext';
import { HomeProvider } from './pages/home/context/HomeContext';

function App() {
  const location = useLocation();
  const isHome = location.pathname === '/'

  return (
    <>
      <ParticularServiceProvider>
        <HomeProvider>
          {isHome && <Header />}
          <Routes>
            <Route exact path='/' Component={Home}>
            </Route>
            <Route path='/services/:serviceSlug' Component={ParticularService}>

            </Route>
            <Route path='/services/:serviceName/:subServiceSlug' Component={ParticularService}></Route>
          </Routes>
          {isHome && <Footer />}
        </HomeProvider>
      </ParticularServiceProvider>
    </>
  )
}

export default App
