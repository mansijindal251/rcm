import axios from "axios";

class HttpClient {
    constructor() {
        this.instance = axios.create({
            // baseURL: 'http://localhost:3000/api/v1',
            baseURL: 'https://api.cwjhealth.com/api/v1',

        });

        this.instance.interceptors.request.use(
            this._handleRequest,
            this._handleRequestError
        );
    }

    _handleRequestError = (error) => {
        return Promise.reject(error);
    };

    get = (url, queryparams, config) => {
        return this.instance.get(url, { params: queryparams, ...config });
    };

    post = (url, data, config) => {
        return this.instance.post(url, data, config);
    }

    put = (url, data, config) => {
        return this.instance.put(url, data, config);
    };

    delete = (url, config) => {
        return this.instance.delete(url, config)
    };

}

export default new HttpClient();